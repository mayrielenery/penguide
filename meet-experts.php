<?php $page='Meet Experts';?>
<?php include "elements/header.php";?>

<section class="cover imagebg height-80 overlay-green inner-cover text-center" data-overlay="9">
	
	<div class="background-image-holder">
		<img src="assets/img/hero-banner-8.jpg">
	</div>
	<div class="container pos-vertical-center ">
		<div class="row justify-content-center mb--80 mb-xs-40">
			<div class="col-12 col-lg-8">
				<h1 class="header-title mt--30">Meet Our Experts</h1>
                <p class="unmarg--bottom">Where appropriate, The Penguide – Dr Chijioke Okorie - works with a consortium specifically assembled in preparation for relevant projects. The consortium is a highly knowledgeable team of experts and researchers, not only in intellectual property (IP) law but also in the areas of media, entertainment, and sports; technology and communications. The consortium converges under Penguide Advisory to undertake relevant projects.</p>
			</div>
		</div>

        
	</div>
	
</section>

<section class="text-left our-team">
        <div class="container">
            <div class="row">
                <div class="col-md-4">

                    <div class="feature feature-1"> 
                    	<img alt="Image" src="assets/img/expert-4.jpg">
                        <div class="feature__body boxed boxed--lg boxed--border">
                            <h3 class="unmarg--bottom">Dr. Desmond Oriakhogba</h3>
                            <p class="mb--20">
                            	Law Faculty, University of Benin 
                            </p>
                            <div class="modal-instance">
                                <a class="modal-trigger" href="#">Learn More </a>
                                <div class="modal-container">
                                    <div class="modal-content height--natural">
                                        <div class="boxed boxed--lg">
                                            <div class="row align-items-center justify-content-around">
                                            	<div class="col-12 col-lg-7 text-justify">
                                            		<h2>Dr. Desmond Oriakhogba</h2>
		                                            <hr class="short">
		                                            <p>
		                                                Called to the Nigerian Bar in 2008. Desmond is a lecturer in the Law Faculty, University of Benin and is currently concluding a postdoctoral research in the University of Cape Town, South Africa (UCT). He is also a Queen Elizabeth Scholar (QES) and Fellow of the Open AIR Network, and is affiliated to the UCT’s DST/NRF/SARChI Chair on IP, Innovation and Development and UCT’s IP Research Unit. Desmond was a visiting researcher at the Centre for Law, Society and Technology (CILT), Law Faculty, University of Ottawa, Canada between August and October 2018. Desmond researches in the broad spectrum of IP law and practice.
		                                            </p>
		                                            <a class="btn bg--facebook btn--icon" href="#">
		                                            	<span class="btn__text"><i class="socicon-linkedin"></i>Read More</span>
		                                            </a>
                                            	</div>
                                            	<div class="col-12 col-lg-4">
                                            		<div class="imagebg image--rounded">
                                            			<div class="background-image-holder">
                                            				<img src="assets/img/expert-4.jpg">
                                            			</div>
                                            		</div>
                                            	</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end of modal instance-->
			            </div>
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="feature feature-1"> 
                    	<img alt="Image" src="assets/img/expert-2.jpg">
                        <div class="feature__body boxed boxed--lg boxed--border">
                            <h3 class="unmarg--bottom">Ifedayo Adeoba</h3>
                            <p class="mb--20">
                            	Attorney at Law
                            </p>
                            <div class="modal-instance">
                                <a class="modal-trigger" href="#">Learn More </a>
                                <div class="modal-container">
                                    <div class="modal-content height--natural">
                                        <div class="boxed boxed--lg">
                                            <div class="row align-items-center justify-content-around">
                                            	<div class="col-12 col-lg-7 text-justify">
                                            		<h2>Ifedayo Adeoba</h2>
		                                            <hr class="short">
		                                            <p>
		                                                Adeoba is a solution driven lawyer with years of experience in legal/regulatory compliance and contract drafting and management (including contract reviews and negotiation).
		                                            </p>
		                                            <a class="btn bg--facebook btn--icon" href="#">
		                                            	<span class="btn__text"><i class="socicon-linkedin"></i>Read More</span>
		                                            </a>
                                            	</div>
                                            	<div class="col-12 col-lg-4">
                                            		<div class="imagebg image--rounded">
                                            			<div class="background-image-holder">
                                            				<img src="assets/img/expert-2.jpg">
                                            			</div>
                                            		</div>
                                            	</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end of modal instance-->
			            </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="feature feature-1"> 
                    	<img alt="Image" src="assets/img/expert-3.jpg">
                        <div class="feature__body boxed boxed--lg boxed--border">
                            <h3 class="unmarg--bottom">Francis Monye</h3>
                            <p class="mb--20">
                            	Computer Science and ICT Expert
                            </p>
                            <div class="modal-instance">
                                <a class="modal-trigger" href="#">Learn More </a>
                                <div class="modal-container">
                                    <div class="modal-content height--natural">
                                        <div class="boxed boxed--lg">
                                            <div class="row align-items-center justify-content-around">
                                            	<div class="col-12 col-lg-7 text-justify">
                                            		<h2>Francis Monye</h2>
		                                            <hr class="short">
		                                            <p>
		                                                Monye is a computer science and ICT expert, with over ten years’ experience as a software developer and computer engineer. He advises diverse technology and digital copyright clients on product design.  Francis holds a first degree in computer science from Lagos State University.
		                                            </p>
		                                            <a class="btn bg--facebook btn--icon" href="#">
		                                            	<span class="btn__text"><i class="socicon-linkedin"></i>Read More</span>
		                                            </a>
                                            	</div>
                                            	<div class="col-12 col-lg-4">
                                            		<div class="imagebg image--rounded">
                                            			<div class="background-image-holder">
                                            				<img src="assets/img/expert-3.jpg">
                                            			</div>
                                            		</div>
                                            	</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end of modal instance-->
			            </div>
                    </div>

                </div>
               	<div class="col-md-4">
                   <div class="feature feature-1"> 
                   	<img alt="Image" src="assets/img/expert-3.jpg">
                       <div class="feature__body boxed boxed--lg boxed--border">
                           <h3 class="unmarg--bottom">Edith Ukofia</h3>
                           <p class="mb--20">
                           	Legal Writing and Legal Research
                           </p>
                           <div class="modal-instance">
                               <a class="modal-trigger" href="#">Learn More </a>
                               <div class="modal-container">
                                   <div class="modal-content height--natural">
                                       <div class="boxed boxed--lg">
                                           <div class="row align-items-center justify-content-around">
                                           	<div class="col-12 col-lg-7 text-justify">
                                           		<h2>Edith Ukofia</h2>
	                                            <hr class="short">
	                                            <p>
	                                                Edith Ukofia has practised law for over a decade, with expertise in legal writing and legal research. Edith has presented various commercial law topics within Nigeria and was one of the advocates for disability rights at a roundtable organised by Nigerian Institute of Advanced Legal Studies (NIALS). She holds a Masters degree in International Commercial Law from the University of Brunel, and a first degree in law from the University of Ibadan. 
	                                            </p>
	                                            <a class="btn bg--facebook btn--icon" href="#">
	                                            	<span class="btn__text"><i class="socicon-linkedin"></i>Read More</span>
	                                            </a>
                                           	</div>
                                           	<div class="col-12 col-lg-4">
                                           		<div class="imagebg image--rounded">
                                           			<div class="background-image-holder">
                                           				<img src="assets/img/expert-3.jpg">
                                           			</div>
                                           		</div>
                                           	</div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <!--end of modal instance-->
			            </div>
                   </div>

               	</div>
            </div>
        </div>
    </section>
<?php include "elements/footer.php";?>