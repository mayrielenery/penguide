<?php $page='Publications';?>
<?php include "elements/header.php";?>

<section class="cover imagebg height-60 overlay-green inner-cover text-center" data-overlay="9">
	
	<div class="background-image-holder">
		<img src="assets/img/hero-banner-7.jpg">
	</div>
	<div class="container pos-vertical-center ">
		<div class="row justify-content-center mb--80 mb-xs-40">
			<div class="col-12 col-lg-8">
				<h2 class="mt--80 unmarg--bottom">Queen Elizabeth Scholar and Open African Innovation Research (Open AIR) Fellowship</h2>
                
			</div>
		</div>
	</div>
	
</section>

<div class="vl mb--50"></div>

<section class="unpad--top">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<p class="text-justify">
					Dr. Chijioke Okorie is a <a href="https://queenelizabethscholars.ca/" class="styled-hyperlink" target="_blank">Queen Elizabeth Scholar</a> (QES) and a postdoctoral Fellow under the <a href="http://www.openair.org.za/" class="styled-hyperlink" target="_blank">Open AIR</a> project. As a 	QES/OAIR Fellow, Dr Okorie’s remit is to conduct research into the existence of gender-infused perspectives in policies that inform intellectual property laws in Africa. She also contributes to the formulation of work for Open AIR’s <a href="http://openair.africa/project/metrics-and-policies/" class="styled-hyperlink" target="_blank">Metrics, Laws, and, Policies</a> theme.
				</p>
				<p class="text-justify">
					The QES program is a project-based scholarship and research program. In response to QES calls for proposals, Canadian universities submit project proposals that include inbound and outbound scholarship and research opportunities in various fields and disciplines. Universities with winning project then recruit scholars and researchers (Canadian outbound or International inbound) based on their area of focus, the partner organizations and partner countries.
				</p>
				<p class="text-justify">
					Open AIR is a unique collaborative network of researchers spread across 15 African countries, Canada, and elsewhere in the world. The Open AIR network received funding from the Canadian Queen Elizabeth II Diamond Jubilee Advanced Scholars Program (QES) to create new opportunities for emerging scholars to explore African innovation through the lens of gender equality and the empowerment of women and girls.
				</p>
				<div class="border-left-gold blockbox">
					<p class="text-justify">
						Under the QES/Open AIR arrangement, Dr. Okorie was Visiting Scholar at the Open AIR hub at the Centre for Law, Technology and Society at the University of Ottawa in Canada, where she presented her research on “Gender equality and social justice through the regulation of collecting societies”.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include "elements/footer.php";?>