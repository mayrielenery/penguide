<?php $page='Publications';?>
<?php include "elements/header.php";?>

<section class="cover imagebg height-60 overlay-green inner-cover text-center" data-overlay="9">
	
	<div class="background-image-holder">
		<img src="assets/img/hero-banner-7.jpg">
	</div>
	<div class="container pos-vertical-center ">
		<div class="row justify-content-center mb--80 mb-xs-40">
			<div class="col-12 col-lg-8">
				<h2 class="mt--80 unmarg--bottom">Research fellowship under the South African Research Chair in IP, Innovation and Development</h2>
                
			</div>
		</div>
	</div>
	
</section>

<div class="vl mb--50"></div>

<section class="unpad--top">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<p class="text-justify">
					The Penguide, Dr. Chijioke Okorie is currently the postdoctoral research fellow under the South African Research Chair in IP, Innovation and Development at the University of Cape Town, South Africa.
				</p>
				<div class="border-left-gold blockbox">
					<p class="text-justify">
						Under the Chair, Dr. Okorie’s work focuses on supporting the priorities of the South African Government’s National Development Plan 2030, to reduce inequality and poverty by inter alia, “building the capability of the state to play a developmental, transformative role”. She explores the question of how IP regulatory institutions and the legal frameworks may be improved and their capabilities strengthened to enable them play a developmental, transformative role in the creative sectors and in national digital economies.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include "elements/footer.php";?>