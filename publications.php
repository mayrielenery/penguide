<?php $page='Publications';?>
<?php include "elements/header.php";?>

<section class="cover imagebg height-60 overlay-green inner-cover text-center" data-overlay="9">
	
	<div class="background-image-holder">
		<img src="assets/img/hero-banner-7.jpg">
	</div>
	<div class="container pos-vertical-center ">
		<div class="row justify-content-center mb--80 mb-xs-40">
			<div class="col-12 col-lg-8">
				<h1 class="header-title mt--30">Projects & Publications</h1>
                
			</div>
		</div>
	</div>
	
</section>

<section>
    <div class="container">
        <div class="row mb--30">
            <div class="col-12">
                <h3 class="mb--8">Projects</h3>
                <hr>
            </div>
        </div>
        
        <div class="row equal-container">
            <div class="col-12 col-md-3">
                <div class="feature feature-1">
                    <img alt="Image" src="assets/img/blog-1.jpg" />
                    <div class="feature__body boxed boxed--border">
                        <div class="equal">
                            <h4 class="mb--24">Research fellowship under the South African Research Chair in IP, Innovation and Development</h4>
                            
                        </div>
                        <div class="modal-instance">
                            <a class="modal-trigger" href="#">Learn More </a>
                            <div class="modal-container w-80">
                                <div class="modal-content height--natural">
                                    <div class="boxed boxed--lg">
                                        <div class="row align-items-center justify-content-around">
                                            <div class="col-12 col-lg-7">
                                                <h2>Research fellowship under the South African Research Chair in IP, Innovation and Development</h2>
                                                <hr class="short">
                                                <p class="text-justify">
                                                    The Penguide, Dr. Chijioke Okorie is currently the postdoctoral research fellow under the South African Research Chair in IP, Innovation and Development at the University of Cape Town, South Africa.
                                                </p>
                                                <p class="text-justify">
                                                    Under the Chair, Dr. Okorie’s work focuses on supporting the priorities of the South African Government’s National Development Plan 2030, to reduce inequality and poverty by inter alia, “building the capability of the state to play a developmental, transformative role”. She explores the question of how IP regulatory institutions and the legal frameworks may be improved and their capabilities strengthened to enable them play a developmental, transformative role in the creative sectors and in national digital economies.
                                                </p>
                                                
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="imagebg image--rounded">
                                                    <div class="background-image-holder">
                                                        <img src="assets/img/expert-4.jpg">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end feature-->
            </div>
            <div class="col-12 col-md-3">
                <div class="feature feature-1">
                    <img alt="Image" src="assets/img/blog-1.jpg" />
                    <div class="feature__body boxed boxed--border">
                        <div class="equal">
                            <h4 class="mb--24">Queen Elizabeth Scholar and Open African Innovation Research (Open AIR) Fellowship</h4>
                            
                        </div>
                        <div class="modal-instance">
                            <a class="modal-trigger" href="#">Learn More </a>
                            <div class="modal-container w-80">
                                <div class="modal-content height--natural">
                                    <div class="boxed boxed--lg">
                                        <div class="row align-items-center justify-content-around">
                                            <div class="col-12 col-lg-7">
                                                <h2>Queen Elizabeth Scholar and Open African Innovation Research (Open AIR) Fellowship</h2>
                                                <hr class="short">
                                                <p class="text-justify">
                                                    Dr. Chijioke Okorie is a Queen Elizabeth Scholar (QES) and a postdoctoral Fellow under the Open AIR project. As a   QES/OAIR Fellow, Dr Okorie’s remit is to conduct research into the existence of gender-infused perspectives in policies that inform intellectual property laws in Africa. She also contributes to the formulation of work for Open AIR’s Metrics, Laws, and, Policies theme.
                                                </p>
                                                <p class="text-justify">
                                                    The QES program is a project-based scholarship and research program. In response to QES calls for proposals, Canadian universities submit project proposals that include inbound and outbound scholarship and research opportunities in various fields and disciplines. Universities with winning project then recruit scholars and researchers (Canadian outbound or International inbound) based on their area of focus, the partner organizations and partner countries.
                                                </p>
                                                <p class="text-justify">
                                                    Open AIR is a unique collaborative network of researchers spread across 15 African countries, Canada, and elsewhere in the world. The Open AIR network received funding from the Canadian Queen Elizabeth II Diamond Jubilee Advanced Scholars Program (QES) to create new opportunities for emerging scholars to explore African innovation through the lens of gender equality and the empowerment of women and girls.
                                                </p>
                                                <p class="text-justify">
                                                    Under the QES/Open AIR arrangement, Dr. Okorie was Visiting Scholar at the Open AIR hub at the Centre for Law, Technology and Society at the University of Ottawa in Canada, where she presented her research on “Gender equality and social justice through the regulation of collecting societies”.
                                                </p>
                                                <p class="text-justify">
                                                    Please visit the QES and Open AIR respective websites for more information. 
                                                </p>
                                               
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="imagebg image--rounded">
                                                    <div class="background-image-holder">
                                                        <img src="assets/img/expert-4.jpg">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end feature-->
            </div>
            <div class="col-12 col-md-3">
                <div class="feature feature-1">
                    <img alt="Image" src="assets/img/blog-1.jpg" />
                    <div class="feature__body boxed boxed--border">
                        <div class="equal">
                            <h4 class="mb--24">Legal Aid Initiatives For Artists And Startups</h4>
                            
                        </div>
                        <div class="modal-instance">
                            <a class="modal-trigger" href="#">Learn More </a>
                            <div class="modal-container w-80">
                                <div class="modal-content height--natural">
                                    <div class="boxed boxed--lg">
                                        <div class="row align-items-center justify-content-around">
                                            <div class="col-12 col-lg-7">
                                                <h2>Legal Aid Initiatives For Artists And Startups</h2>
                                                <hr class="short">
                                                <p class="text-justify">
                                                    Established in 2018, The Legal Aid Initiatives for Artists and Start-ups (The Initiative) is a registered non-governmental organisation (NGO), which aims to achieve two broad objectives
                                                </p>
                                                <p class="type--bold unmarg--bottom">
                                                    Legal and transactional IP advice for artists and start-ups

                                                </p>
                                                <p class="text-justify"> To achieve this objective, The Initiative provides pro bono legal and ancillary advice to independent artists including but not limited to visual artists, musicians, performers, film makers, game designers, programmers, etc. and creative entrepreneurs and corporate startups.</p>

                                                <p class="type--bold unmarg--bottom">
                                                    Clinical IP education
                                                </p>
                                                <p class="text-justify"> The Initiative provides a platform for young and other willing lawyers to re-learn and learn the ropes of intellectual property law practice and offers pro bono clinical IP training in Intellectual Property and related practice for law students.</p>
                                                
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="imagebg image--rounded">
                                                    <div class="background-image-holder">
                                                        <img src="assets/img/expert-4.jpg">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end feature-->
            </div>

            <div class="col-12 col-md-3">
                <div class="feature feature-1">
                    <img alt="Image" src="assets/img/blog-1.jpg" />
                    <div class="feature__body boxed boxed--border">
                        <div class="equal">
                            <h4 class="mb--24">Conversation on The Digital Creative Economy: Copyright, Law and Policy in Africa</h4>
                            
                        </div>
                        <div class="modal-instance">
                            <a class="modal-trigger" href="#">Learn More </a>
                            <div class="modal-container w-80">
                                <div class="modal-content height--natural">
                                    <div class="boxed boxed--lg">
                                        <div class="row align-items-center justify-content-around">
                                            <div class="col-12 col-lg-7">
                                                <h2>Conversation on The Digital Creative Economy: Copyright, Law and Policy in Africa</h2>
                                                <hr class="short">
                                                <p class="text-justify">
                                                    With her book – Multi-sided Music Platforms and the Law - Dr. Chijioke Okorie has opened up <a href="https://www.afronomicslaw.org/book-symposia/" class="styled-hyperlink" target="_blank">conversations</a> on the application of copyright, privacy and competition laws to the digital creative economy in Africa.
                                                </p>
                                                <p class="text-justify">
                                                    As part of the public presentation of the book, Dr. Chijioke Okorie will be discussing various aspects of the book with Professor of Media Studies, <a href="http://www.cfms.uct.ac.za/fam/staff/haupt" class="styled-hyperlink" target="_blank">Adam Haput</a> on 12th February 2020 at the Kramer Building, Faculty of Law, University of Cape Town.
                                                </p>
                                                <p>
                                                    There will also be a wider panel discussion on “The Digital Creative Economy: Copyright, Law and Policy in Africa” in Lagos, Nigeria on 11th March 2020.
                                                </p>
                                                <p>
                                                    See flyers for details!      
                                                </p>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="imagebg image--rounded">
                                                    <div class="background-image-holder">
                                                        <img src="assets/img/expert-4.jpg">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end feature-->
            </div>
        </div>
    </div>
</section>

<section class="bg--secondary publications border-0">
    <div class="container">
        <div class="row mb--30">
            <div class="col-12">
                <h3 class="mb--8">Publications</h3>
                <hr class="mt--8">

            </div>
            
        </div>
        <div class="list-of-publications">
            <div class="row">
                <div class="col-12">
                    <p class="type--bold label">Book(s)</p>
                    <ol>
                        <li>
                            <span class="pl--8">Multi-sided music platforms and the law: Copyright, law and policy in Africa (Routledge: 2020).</span>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="list-of-publications">
            <div class="row">
                <div class="col-12">
                    <p class="type--bold label">Peer-reviewed research and/or policy publications</p>
                    <ol>
                        <li>
                            <span class="pl--8">Screenplays and feature film production: Copyright protection and enforcement. Business Law Review (Issue 2) 2020 (forthcoming).</span></li>
                        <li>
                            <span class="pl--8"><a href="https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3489093" class="styled-hyperlink" target="_blank">Corporate governance of copyright collecting societies in Nigeria: are (some) interventions under the Copyright Act lawful?</a> <i>Nigerian Institute of Advanced Legal Studies Journal of Intellectual Property</i> (forthcoming). </span>
                        </li>
                        <li>
                            <span class="pl--8"><a href="https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3489093https://academic.oup.com/jiplp/advance-article-abstract/doi/10.1093/jiplp/jpz133/5603536?redirectedFrom=fulltext" class="styled-hyperlink" target="_blank">Copyright in “contest-created” works: Ugandan Court of Appeal weighs private interests and public benefit vis-à-vis Uganda’s national anthem</a> <i>Journal of Intellectual Property Law & Practice</i>, jpz133. </span>
                        </li>

                        <li>
                            <span class="pl--8"><a href="https://academic.oup.com/jiplp/article-abstract/14/8/613/5520002?redirectedFrom=PDF" class="styled-hyperlink" target="_blank">An analysis of the IP-related provisions of the Nigerian Federal Competition and Consumer Protection Act 2019.</a> <i>Journal of Intellectual Property Law & Practice, 14(8)</i>, p. 613-621. </span>
                        </li>   
                        <li>
                            <span class="pl--8"><a href="https://journals.co.za/content/journal/10520/EJC-126f21282d" class="styled-hyperlink" target="_blank">Corporate governance of collecting societies in Nigeria: Powers of the sector regulator.</a> <i>South African Intellectual Property Law Journal, 6(1)</i>, pp.24-46. </span>
                        </li>
                        <li>
                            <span class="pl--8"><a href="https://academic.oup.com/jiplp/article-abstract/13/12/931/5126433?redirectedFrom=fulltext" class="styled-hyperlink" target="_blank">Nigerian Supreme Court issues guidance on locus standi of collecting societies.</a> <i>Journal of Intellectual Property Law & Practice, 13(12)</i>, pp.931-933. </span>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "elements/footer.php";?>