<?php $page='Our Services';?>
<?php include "elements/header.php";?>

<section class="cover imagebg height-80 overlay-green about-us text-center" data-overlay="9">
    
    <div class="background-image-holder">
        <img src="assets/img/hero-banner-4.jpg">
    </div>
    <div class="container pos-vertical-center ">
        <div class="row justify-content-center mb--80 mb-xs-40">
            <div class="col-12 col-lg-8">
                <h1 class="header-title mt--30">Our Services</h1>
                <p class="unmarg--bottom">Penguide Advisory recognizes the value of IP for creators and users. We understand the media and entertainment sector and the digital economy, particularly in the African context. We provide cutting edge analysis and insights for countries, individuals and organisations with whom we work. </p>
            </div>
        </div>

        
    </div>
    
</section>
<section>
	<div class="container">
		

		<div class="row justify-content-between align-items-center list-of-services">
			<div class="col-12 col-md-5">
                <h1 class="color--primary">Intellectual Property Including </h1>
                
				<ul class="list">
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Copyright and related rights
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Designs
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Trademarks
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Trade secrets Marketing
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Proprietary confidential information
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Patents
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Transactional IP
                    </li>

                </ul>
			</div>
            <div class="col-12 col-lg-6 text-center">
                <img src="assets/img/services-2.jpg">
                <a href="#" target="_blank" class="btn btn--gold">
                    <span class="btn__text type--uppercase">Schedule a consultation</span>
                </a>
            </div>
		</div>
	</div>
</section>

<section>
    <div class="container">
        

        <div class="row justify-content-between align-items-center list-of-services">
            <div class="col-12 col-lg-6 text-center">
                <img src="assets/img/services-3.jpg">
                <a href="#" target="_blank" class="btn btn--gold">
                    <span class="btn__text type--uppercase">Schedule a consultation</span>
                </a>
            </div>
            <div class="col-12 col-md-5">
                <h1 class="color--primary">Media, Entertainment and Sport </h1>
                <ul class="list">
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Music
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> TV and Films
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Games
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Social media
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Sport
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Broadcasting
                    </li>

                </ul>
            </div>
            
        </div>
    </div>
</section>



<section>
    <div class="container">
        
        <div class="row justify-content-between align-items-center list-of-services">
            <div class="col-12 col-md-5">
                <h1 class="color--primary">Technology and Communications </h1>
                <ul class="list">
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Software and Services
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Digital Communications
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Fintech
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Edtech
                    </li>
                    <li class="list-item">
                        <i class="icon color-gold material-icons icon--xs">arrow_forward</i> Agritech
                    </li>
                   

                </ul>
            </div>
            <div class="col-12 col-lg-6 text-center">
                <img src="assets/img/services-4.jpg">
                <a href="#" target="_blank" class="btn btn--gold">
                    <span class="btn__text type--uppercase">Schedule a consultation</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="space--sm switchable switchable--switch">
        <div class="container">
            <div class="row text-center justify-content-center">
                <div class="col-md-12">
                    <div class="height-50 imagebg overlay-gold text-center row justify-content-center" data-overlay="7">
                        <div class="background-image-holder"><img alt="background" src="assets/img/services-1.jpg"></div>
                        <div class="pos-vertical-center col-12 col-md-8 justify-content-center boxed boxed--lg bg--none">
                            <h1 class="header-title color-black"><i>“Law without justice is a wound without a cure.”</i></h1>
                            <h4 class="color-black type--uppercase">WILLIAM SCOTT DOWNEY</h4>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include "elements/footer.php";?>