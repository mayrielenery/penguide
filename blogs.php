<?php $page='Events';?>
<?php include "elements/header.php";?>

<section class="cover imagebg height-60 overlay-green inner-cover with-side-line text-center" data-overlay="8">
    
    <div class="background-image-holder">
        <img src="assets/img/hero-banner-7.jpg">
    </div>
    <div class="container pos-vertical-center justify-content-center ">
        <div class="row row-ml-2">
            <div class="col-12">
                <h1 class="title-1 header-title  unmarg--bottom">Our Insights</h1>
            </div>
        </div>
    </div>
    
</section>
<section class="switchable feature-large featured-blog ">
    <div class="container">
        <div class="row justify-content-around boxed">
            <div class="col-md-6">
                <img alt="Image" class="border--round box-shadow" src="assets/img/blog-4.jpg" />
            </div>
            <div class="col-md-6 col-lg-5">
                <div class="switchable__text">
                	<span class="label">Featured Blog</span>
                    <h3>Chams Plc and another v Mastercard Asia/Pacific Pte Ltd and 5 Others – Where a contractual breach involves the misuse of IP-based confidential information (The IPKat, 28 November 2019</h3>
                    
                    <a href="https://ipkitten.blogspot.com/2019/11/chams-plc-and-another-v-mastercard.html" target="_blank">Learn More &raquo;</a>
                </div>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>

<section class="unpad other-blog">
	<div class="container">
		<div class="row equal-container">
			<div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                		<p>
	                        	25 October 2019
		                    </p>
		                    <h4>The African Regional Intellectual Property Organisation (ARIPO) Model Law on Copyright and Related Rights </h4>
	                   
	                	</div>
	                    <a href="http://ipkitten.blogspot.com/2019/10/the-african-regional-intellectual.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
			</div>
			<!-- end of col -->

			<div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->

	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        1 October 2019
	                		    </p>
	                		    <h4>Kenya amends its Copyright Act to ratify the Marrakesh Treaty and address a myriad of other issues</h4>
	                	</div>
	                    
	                    <a href="http://ipkitten.blogspot.com/2019/10/kenya-amends-its-copyright-act-to.html#" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
			</div>
			<div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        17 September 2019
	                		    </p>
	                		    <h4>Who owns copyright in Uganda’s National Anthem?</h4>
	                		    
	                	</div>
	                    <a href="https://ipkitten.blogspot.com/2019/09/who-owns-copyright-in-ugandas-national.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
			</div>
			
			<div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        5 September 2019
	                		    </p>
	                		    <h4>Can Africa’s trade agreements handle regional integration?</h4>
	                		    
	                	</div>
	                    <a href="https://ipkitten.blogspot.com/2019/09/can-africas-trade-agreements-handle.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
			</div>

			<div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        21 August 2019
	                		    </p>
	                		    <h4>Screenplay litigation in Nigeria: Missed opportunities in Raconteur Productions Limited v Dioni Visions Entertainment Limited and Others</h4>
	                		    
	                	</div>
	                    <a href="https://ipkitten.blogspot.com/2019/08/screenplay-litigation-in-nigeria-missed.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
			</div>
			<div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        31 July 2019
	                		    </p>
	                		    <h4>Beyond exclusion of pharmaceutical products from patentable subject matter as a solution to limited access to medicines in Africa</h4>
	                		    
	                	</div>
	                    <a href="https://ipkitten.blogspot.com/2019/07/beyond-exclusion-of-pharmaceutical.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->

			</div>
            <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        3 June 2019
	                		    </p>
	                		    <h4>Will Marvel Studios Face Copyright Infringement Claim For Using Ghanaian Kente Designs In The Black Panther Movie?'</h4>
	                		    
	                	</div>
	                    <a href="https://ipkitten.blogspot.com/2019/06/will-marvel-studios-face-copyright.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>
	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        26 May 2019
	                		    </p>
	                		    <h4>Obituary piracy and what it means for copyright’</h4>
	                		    
	                	</div>
	                    <a href="https://ipkitten.blogspot.com/2019/05/obituary-piracy-and-what-it-could-mean.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>
	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        14 May 2019
	                		    </p>
	                		    <h4>The Agreement on African Continental Free Trade Area (AfCFTA) - Protocol on IP’ </h4>
	                		    
	                	</div>
	                    <a href="http://ipkitten.blogspot.com/2019/05/the-agreement-on-african-continental.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>
	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        5 April 2019
	                		    </p>
	                		    <h4>Access to Copyright Protected Works by Persons with Disabilities – Thoughts on the WIPO SCCR Scoping Study’ </h4>
	                		    
	                	</div>
	                    <a href="https://ipkitten.blogspot.com/2019/04/access-to-copyright-protected-works-by.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>
	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        5 April 2019
	                		    </p>
	                		    <h4>The proposed fair use exception under South Africa’s Copyright Amendment Bill’ </h4>
	                		    
	                	</div>
	                    <a href="http://ipkitten.blogspot.com/2019/03/the-proposed-fair-use-exception-under.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>
	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        February 2019
	                		    </p>
	                		    <h4>Dutch court rules that patents based on Ethiopian Teff flour lack inventiveness - Ancientgrain BV and Bakels Senior NV’ </h4>
	                		    
	                	</div>
	                    <a href="http://ipkitten.blogspot.com/2019/02/dutch-court-rules-that-patents-based-on.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>
	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        February 2019
	                		    </p>
	                		    <h4>Exploiting arrangements of traditional (gospel or folk) music in South Africa’</h4>
	                		    
	                	</div>
	                    <a href="http://ipkitten.blogspot.com/2019/02/exploiting-arrangements-of-traditional.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>
	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        14 January 2019
	                		    </p>
	                		    <h4>Technicalities in copyright litigation in Nigeria: MCSN v Compact Disc Technology’</h4>
	                		    
	                	</div>
	                    <a href="http://ipkitten.blogspot.com/2019/01/technicalities-in-copyright-litigation.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>

	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        11 October 2018
	                		    </p>
	                		    <h4>Intellectual property and crime: The Nigerian Copyright Commission files criminal charges against a music collecting society’ </h4>
	                		    
	                	</div>
	                    <a href="https://afro-ip.blogspot.com/2018/10/intellectual-property-and-crime_11.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>

	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        13 August 2018
	                		    </p>
	                		    <h4>Adeokin Records v Music Copyright Society of Nigeria: No CMO licence required for an exclusive licensee of copyright to enforce its licence’ </h4>
	                		    
	                	</div>
	                    <a href="https://the1709blog.blogspot.com/2018/08/adeokin-records-v-music-copyright.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>

	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        9 August 2018
	                		    </p>
	                		    <h4>Trademark infringement in Nigeria: What is “use in the course of trade”? </h4>
	                		    
	                	</div>
	                    <a href="http://afro-ip.blogspot.com/2018/08/trademark-infringement-in-nigeria-what_9.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>

	        <div class="col-md-4">
				<div class="feature feature-1">
	                <!-- <img alt="Image" src="assets/img/blog-1.jpg" /> -->
	                <div class="feature__body boxed boxed--border">
	                	<div class="equal">
	                			<p>
	                		        14 January 2019
	                		    </p>
	                		    <h4>Open Educational Resources – a Nigerian perspective ‘ (Afro-IP, 13 June 2013)</h4>
	                		    
	                	</div>
	                    <a href="http://afro-ip.blogspot.com/2013/06/open-educational-resources-nigerian.html" target="_blank">
	                        Learn More
	                    </a>
	                </div>
	            </div>
	            <!--end feature-->
	        </div>
		</div>
	</div>
</section>

<?php include "elements/footer.php";?>