<?php $page='Contact Us';?>
<?php include "elements/header.php";?>

<section class="cover imagebg height-80 overlay-green inner-cover with-side-line " data-overlay="8">
    
    <div class="background-image-holder">
        <img src="assets/img/hero-banner-2.jpg">
    </div>
    <div class="container pos-vertical-center ">
        <div class="row row-ml-2">
            <div class="col-12">
                <h1 class="title-1 header-title  unmarg--bottom mt--30">Contact Us</h1>
            </div>
        </div>
    </div>
    
</section>


    <section class="switchable">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-5"> <img alt="Image" class="border--round box-shadow-wide" src="assets/img/contact-1.jpg"> </div>
                <div class="col-md-6">
                    <div class="row switchable__text">
                        <div class="col-12">
                            <p class="lead"> E: <a href="#">info@penguide.com</a><br> P: +234 - 14531010 </p>
                            <p class="lead"> Give us a call or drop by anytime, we endeavour to answer all enquiries within 24 hours on business days. </p>
                            <p class="lead"> We are open from 9am — 5pm week days. </p>
                            
                            <form class="form-email row" data-success="Thanks for your enquiry, we'll be in touch shortly." data-error="Please fill in all fields correctly.">
                                <div class="col-md-6"> <label>Your Name:</label> <input type="text" name="Name" class="validate-required"> </div>
                                <div class="col-md-6"> <label>Email Address:</label> <input type="email" name="email" class="validate-required validate-email"> </div>
                                <div class="col-md-12"> <label>Message:</label> <textarea rows="4" name="Message" class="validate-required"></textarea> </div>
                                <div class="col-md-5 col-lg-4"> <button type="submit" class="btn btn--primary type--uppercase">Send Enquiry</button> </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include "elements/footer.php";?>