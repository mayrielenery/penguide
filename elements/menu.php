        <div class="bar--absolute bar--transparent">
            <section class="bar bar-3 bar--sm hidden-sm " id="top-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="bar__module mb-xs-0">
                                <ul class="menu-horizontal left">
                                    
                                    
                                    <li>
                                        <a href="#" target="_blank">
                                           Lagos - Nigeria
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank">
                                           info@penguide.com
                                        </a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right text-left-xs text-left-sm hidden-xs">
                            <div class="bar__module">
                                <ul class="menu-horizontal">
                                
                                
                                    <li>
                                        <a href="#" target="_blank">
                                            <i class="socicon-facebook "></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank">
                                            <i class="socicon-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="socicon-linkedin"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="socicon-google"></i>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <!--end bar-->
            
            <!--end of notification-->
            <div class="nav-container ">
                
                <nav id="menu1" class="bar bar--transparent bar-sm bar-1 hidden-sm hidden-xs" data-scroll-class='70px:pos-fixed'>
                    <div class="container">
                        <div class="row align-item-center">
                            <div class="col-lg-4 col-md-2 hidden-xs">
                                <div class="bar__module">
                                    <a href="index">
                                        <img class="logo logo-dark" alt="logo" src="assets/img/logo-dark.png" />
                                        <img class="logo logo-light" alt="logo" src="assets/img/logo-white.png" />
                                    </a>
                                </div>
                                <!--end module-->
                            </div>
                            <div class="col-lg-8 col-md-12 text-right text-left-xs text-left-sm align-self-center">
                                <div class="bar__module">
                                    <ul class="menu-horizontal">
                                        <li> 
                                            <a href="index">
                                                <i class="icon color--light material-icons icon--sm v-align-middle">home</i>
                                            </a> 
                                        </li>
                                        
                                        <li> 
                                            <a href="about-us">
                                                About Us
                                            </a> 
                                        </li>
                                        <li> 
                                            <a href="publications">
                                                Publications
                                            </a> 
                                        </li>
                                        <li> 
                                            <a href="our-services">
                                                Our Services
                                            </a> 
                                        </li>
                                        <li> 
                                            <a href="meet-experts">
                                                Our Experts
                                            </a> 
                                        </li>
                                        <li> 
                                            <a href="contact-us">
                                               Contact Us
                                            </a> 
                                        </li>
                                    </ul>
                                </div>
                                <!--end module-->
                                
                            </div>
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of container-->
                </nav>
                <!--end bar-->
                
            </div>

            <nav class="bar bar-toggle bar--transparent hidden-lg hidden-md visible-sm visible-xs" data-scroll-class='70px:pos-fixed' data-fixed-at="200">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col">
                                <div class="bar__module logo-style">
                                    <a href="index"> 
                                        <img class="logo logo-dark hidden-sm hidden-xs" alt="logo" src="assets/img/logo-dark.png">
                                        <img class="logo logo-light" alt="logo" src="assets/img/logo-white.png">  </a>
                                </div>
                                <!--end module-->
                            </div>
                            <div class="col d-flex justify-content-end">
                                <div class="bar__module">
                                    <a class="menu-toggle pull-right" href="#" data-notification-link="sidebar-menu">
                                        <i class="stack-interface stack-menu"></i>
                                    </a>
                                    
                                   
                                </div>

                                <!--end module-->
                            </div>
                            <!--end columns-->
                        </div>
                        <!--end of row-->
                        <div class="notification pos-right pos-top side-menu bg--dark hidden-lg hidden-md visible-sm visible-xs" data-notification-link="sidebar-menu" data-animation="from-right">
                            <div class="side-menu__module pos-vertical-center text-center">
                                <ul class="menu-vertical">
                                    <li class="mb-xs-40">
                                        <a href="index">
                                            <img class="logo" alt="logo" src="assets/img/logo-white.png" />
                                            
                                        </a>
                                    </li>
                                    <li>
                                        <a href="index">Home</a>
                                    </li>
                                    <li> 
                                        <a href="about-us">
                                            About Us
                                        </a> 
                                    </li>
                                    <li> 
                                        <a href="publications">
                                            Publications
                                        </a> 
                                    </li>
                                    <li> 
                                        <a href="our-services">
                                            Our Services
                                        </a> 
                                    </li>
                                    <li> 
                                        <a href="meet-experts">
                                            Our Experts
                                        </a> 
                                    </li>
                                    <li> 
                                        <a href="contact-us">
                                           Contact Us
                                        </a> 
                                    </li>
                                </ul>
                                <ul class="menu-horizontal text-center">
                                       
                                        
                                        <li> 
                                            <a href="#" class="type--lowercase">
                                                <i class="icon icon--xs color--black icon-Email mr--1 v-align-middle"></i> info@penguide.com
                                            </a> 
                                        </li>
                                        <hr>
                                        <li>
                                            <a href="#">
                                                <span class="type--fine-print">PenGuide © <span class="update-year"></span>. All Rights Reserved.</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="">
                                            </a>
                                        </li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                    <!--end of container-->
            </nav>
        </div>
        <div class="main-container">