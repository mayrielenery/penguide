<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Penguide</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
        <link href="assets/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/extras.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/theme-greensea.css" rel="stylesheet" type="text/css" media="all" />
        <link href="assets/css/global.css" rel="stylesheet" type="text/css" media="all" />

        <?php if($page=="Home") { ?>
            <link href="assets/css/home.css?v=<?php echo(rand(1,10));?>" rel="stylesheet" type="text/css" media="all" />
        <?php } ?>
         <?php if($page=="Our Services") { ?>
            <link href="assets/css/our-services.css?v=<?php echo(rand(1,10));?>" rel="stylesheet" type="text/css" media="all" />
        <?php } ?>

        <?php if($page=="About Us") { ?>
            <link href="assets/css/about-us.css?v=<?php echo(rand(1,10));?>" rel="stylesheet" type="text/css" media="all" />
        <?php } ?>
        <?php if($page=="Meet Experts") { ?>
            <link href="assets/css/meet-experts.css?v=<?php echo(rand(1,10));?>" rel="stylesheet" type="text/css" media="all" />
        <?php } ?>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css" media="all" />

        <link href="https://fonts.googleapis.com/css?family=EB+Garamond:400,600|Montserrat:400,500&display=swap" rel="stylesheet">

    </head>
    <body data-smooth-scroll-offset="77">
        <?php include "menu.php";?>