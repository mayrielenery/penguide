            <section class="height-30 hidden-xs">
            </section>
            <section class="bg--secondary with-side-line  newsletter-container border-0">
                <div class="container">
                   <div class="row ">
                       <div class="col-12">
                           <div class="bg-primary newsletter-box pos-absolute">
                                <h1 class="title header-title mb--80 mb-xs-40 color-gold"> Stay In Touch</h1>
                                <form class="row justify-content-center" action="//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for subcribing.  Please check your inbox." data-error="Please provide your, email address and message.">
                                    <div class="col-lg-9 col-md-6 unmarg--bottom">
                                        <input class="validate-required unmarg--bottom mb-xs-8" type="text" name="email" placeholder="Email Address"> 
                                    </div>

                                    <div class="col-lg-3 col-md-6"> <button type="submit" class="btn btn--lg btn-dark-primary type--uppercase unmarg--top">Subscribe Now</button> </div>
                                </form>
                           </div>
                       </div>
                   </div>
                </div>
                
            </section>
            <footer class="space--xs border-0 bg--secondary text-center-xs footer-2">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-sm-6 col-md-3 col-xs-6">
                            <img src="assets/img/logo-dark.png">
                        </div>
                        <div class="col-sm-6 col-md-2 col-xs-6 mb-xs-30">
                            <h3 class="footer-title ">Quick Links</h3>
                            <ul class="list--hover">
                                <li><a href="index">Home</a></li>
                                <li><a href="about-us">About Us</a></li>
                                <li><a href="our-services">Our Services</a></li>
                                <li><a href="#">Case Studies</a></li>
                                <li><a href="contact-us">Contact Us</a></li>
                                <li><a href="#">Pages</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-2 col-xs-6 mb-xs-30">
                            <h3 class="footer-title ">Our Services</h3>
                            <ul class="list--hover">
                                <li><a href="#">Legal</a></li>
                                <li><a href="#">Transactional Advisory</a></li>
                                <li><a href="#">Training</a></li>
                                <li><a href="#">Capacity Building</a></li>
                                <li><a href="#">Research Services</a></li>
                                <li><a href="#">Policy Advisory</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3 col-xs-6 mb-xs-30">
                            <h3 class="footer-title ">Head Office</h3>
                            <ul class="list--hover">
                                <li><a href="#">Penguide Advisory</a></li>
                                <li><a href="#">2nd Floor, Suite 203E,</a></li>
                                <li><a href="#">Dew International Complex</a></li>
                                <li><a href="#">Omorinre Johnson Street,</a></li>
                                <li><a href="#">Lekki Phase 1, Lagos - Nigeria.</a></li>
                                 <li><a href="#"><i class="material-icons icon--xs mr--16 v-align-middle">phone</i>+234 - 14531010</a></li>
                                 <li><a href="#"><i class="material-icons icon--xs mr--16 v-align-middle">email</i>info@penguideng.com</a></li>
                                 <li><a href="#"><i class="material-icons icon--xs mr--16 v-align-middle">pin_drop</i>P.O Box 55627, Ikoyi</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row border-top">
                        
                        <div class="col-sm-6 mt--20 ">
                            <ul class="social-list list-inline list--hover">
                                <li><a href="#"><i class="socicon socicon-google icon icon--xs"></i></a></li>
                                <li><a href="#"><i class="socicon socicon-twitter icon icon--xs"></i></a></li>
                                <li><a href="#"><i class="socicon socicon-facebook icon icon--xs"></i></a></li>
                                <li><a href="#"><i class="socicon socicon-instagram icon icon--xs"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 mt--20 text-right text-left-xs"> <span class="type--fine-print">© <span class="update-year"></span> Penguide Associates. All rights reserved. </span>
                    </div>
                </div>
            </footer>
        </div>
        <script src="assets/js/jquery-3.1.1.min.js"></script>
        <script src="assets/js/parallax.js"></script>
        <script src="assets/js/flickity.min.js"></script>
        <script src="assets/js/isotope.min.js"></script>
        <script src="assets/js/smooth-scroll.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        <script src="assets/js/helpers.min.js"></script>

    </body>

</html>