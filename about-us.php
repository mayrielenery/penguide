<?php $page='About Us';?>
<?php include "elements/header.php";?>

<section class="cover imagebg height-80 overlay-green about-us text-center" data-overlay="9">
	
	<div class="background-image-holder">
		<img src="assets/img/bg-image.jpg">
	</div>
	<div class="container pos-vertical-center ">
		<div class="row justify-content-center mb--80 mb-xs-40">
			<div class="col-12 col-lg-8">
				<h1 class="header-title mt--30">About Penguide</h1>
                <p class="unmarg--bottom">Penguide provides specialist legal Advisory, Research and Training Services to individuals, firms and the public organisations in the copyright, technology and internet sectors. The firm consists of experts in law and technology, both at local and international levels. Our multi-jurisdictional network and research experience ensure that we provide our clients with practical, globally applicable solutions to their legal issues and needs.</p>
			</div>
		</div>

        
	</div>
	
</section>

    <section class="expert pb-xs-0">
        <div class="container">
            
            <div class="row justify-content-between align-items-center ">
                
                <div class="col-12 col-sm-5 col-lg-5 box-expert">
                    <div class="feature feature-1 unmarg--bottom"> <img alt="Image" src="assets/img/expert-1.jpg">
                        <div class="feature__body boxed boxed--border unmarg--bottom">
                            <a href="#">
                                <h3 class="unmarg--bottom name fancy"><span> Dr. Chijioke Okorie</span></h3>
                                <p class="type--uppercase ml--30"> Lagos, Nigeria </p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 text-justify">
                    <h2 class="header-title color--primary fancy"><span>The Penguide</span></h2>
                    <p>The Penguide is the online persona of  <strong>Dr. Chijioke Ifeoma Okorie</strong>.</p>
                     <div class="col-12 col-lg-9 unpad">
                        <p class="mb--35">Chijioke Okorie is currently the inaugural postdoctoral research fellow under the South African Research Chair in Intellectual Property, Innovation and Development at the University of Cape Town, South Africa. Before becoming a full time researcher and policy consultant, she was a Senior Associate in the law firm of CLP Legal (formerly, Consolex Legal Practitioners).</p>
                    </div>
                    
                </div>
               
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-12 col-sm-5 col-lg-6 text-justify">
                    <p>Chijioke has written extensively in the IP field, and her monograph – Multi-sided Music Platforms and the Law: Copyright, Law and Policy in Africa (Routledge: 2020) – discusses how African countries can apply copyright, privacy and competition laws to regulate the ways in which multi-sided music platforms use copyright-protected content in digital advertising.</p>
                </div>
                <div class="col-12 col-sm-6 col-lg-5 ">
                    <img src="assets/img/banner-img-2.png">
                </div>
            </div>
        </div>
    </section>

    <section class="space--sm ">
        <div class="container">
            <div class="row text-center justify-content-center mb--40">
                <div class="col-md-12">
                    <div class="height-70 imagebg text-center row justify-content-center">
                        <div class="background-image-holder"><img alt="background" src="assets/img/about-1.jpg"></div>
                       
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12  mb--80">
                    <p>Chijioke comments on African IP issues for award-winning IP blogs The IPKat, where she is the Africa Correspondent and Afro-IP, for which she has authored several posts over the past few years. She contributes to research and policy debates on IP and internet law issues and her research has been published in several reputable journals such as Journal of Intellectual Property Law and Practice; South African Intellectual Property Law Journal; Business Law Review etc. </p>
                    <p>Highly regarded in African IP law circles, plays an active role in various professional associations and participates in developmental research projects. She has been invited to give lectures and seminars in IP to various constituencies including creative entrepreneurs and aspiring IP lawyers as well as being consulted by private organisations, government and institutional sectors. She co-supervises postgraduate research students and joins in teaching Electronic IP Law and, Electronic Transactions Law to LLM students at UCT.</p>
                    <p>Chijioke Okorie’s work at Penguide Advisory includes handling the legal/business Affairs of a number of Nigeria’s leading and best-known entertainment industry artists, stylists, producers and labels such as Flavour, Masterkraft, Style Your Selfie, Zhymma, 2nite Music Group etc. </p>
                </div>

                <div class="col-12 pl--80 pl-xs-16 mb--80">
                    <h2 class=" color--primary fancy"><span>Education</span></h2>
                    <ul class="bullets">
                        <li class="mb--16">University of Cape Town, PhD (Digital copyright and competition laws)</li>
                         <li>University of Strathclyde, LLM (Internet Law and Policy)</li>
                    </ul>
                </div>

                <div class="col-12 pl--80 pl-xs-16">
                    <h2 class=" color--primary fancy"><span>Bar Admissions and Professional Associations</span></h2>
                    <ul class="bullets">
                        <li class="mb--16">Advocate and Solicitor of the Supreme Court of Nigeria (2006)</li>
                        <li class="mb--16">International Association for the Advancement of Teaching and Research in Intellectual Property (ATRIP), </li>
                        <li class="mb--16">South African Association of Intellectual Property Law and Information Technology Law Teachers and Researchers (AIPLITL) </li>
                        <li class="mb--16">Nigerian Bar Association</li>
                        <li class="mb--16">Section on Business Law, Nigerian Bar Association</li>
                        <li class="mb--16">Society of International Economic Law</li>
                        <li class="mb--16">Institute of Chartered Mediators and Conciliators </li>
                    </ul>
                </div>

            </div>
        </div>
    </section>

<?php include "elements/footer.php";?>