<?php $page='Home';?>
<?php include "elements/header.php";?>
        
        <section class="cover cover-features imagebg height-100 slider" id="home-banner">
            <ul class="slides">
                <li class="imagebg image--dark overlay-green" data-overlay="8" >
                    <div class="background-image-holder"> 
                        <img alt="background" src="assets/img/hero-banner-3.jpg"> 
                    </div>
                            
                    <div class="container pos-vertical-center text-center-xs">
                        <div class="row mt--20">
                            <div class="col-12 col-md-12 col-lg-7">
                                <p class="text-top type--uppercase mb-xs-10">Welcome to Penguide advisory</p>
                                <h1 class="header-title">Copyright, Internet and Technology Law Consultancy.</h1>
                                <p class="mb--40 mb-xs-20">Penguide provides research, policy and legal advisory services to creative start-ups and local and international organisations.</p>
                                <a href="#" class="btn btn--gold btn--lg">
                                    <span class="btn__text type--uppercase">Learn More</span>
                                </a>
                               
                            </div>
                            
                        </div>
                        
                    </div>
                </li>
                <li class="imagebg image--dark overlay-green" data-overlay="8" >
                    <div class="background-image-holder"> 
                        <img alt="background" src="assets/img/hero-banner-1.jpg"> 
                    </div>
                            
                    <div class="container pos-vertical-center text-center-xs side-image">
                        <div class="row align-items-center mt--20">
                            <div class="col-12 col-md-6 col-lg-7">
                                <p class="text-top mb-xs-10 type--uppercase">Avail best selling book</p>
                                <h1 class="header-title"> Multi-sided music platforms and the Law – Copyright, Law and Policy in Africa</h1>
                               <a href="#" class="btn btn--gold btn--lg">
                                    <span class="btn__text type--uppercase">Learn More</span>
                                </a>
                            </div>
                            <div class="col-12 col-md-5 col-lg-5 text-center-xs">
                                <img src="assets/img/banner-img-2.png">
                            </div>
                            
                        </div>
                        
                    </div>
                </li>
                <li class="imagebg image--dark overlay-green" data-overlay="8" >
                    <div class="background-image-holder"> 
                        <img alt="background" src="assets/img/hero-banner-5.jpg"> 
                    </div>
                            
                    <div class="container pos-vertical-center text-center-xs">
                        <div class="row mt--20">
                            <div class="col-12 col-md-12 col-lg-7">
                                <p class="text-top mb-xs-10 type--uppercase">Emergence of the Internet and developments</p>
                                <h1 class="header-title"> Intellectual Property (IP), Internet Laws and Development in Africa</h1>
                                <p class="mb--40 mb-xs-20">Penguide Advisory’s work spans several topical issues in the areas of IP and Internet laws, with a focus on Africa and national issues.</p>
                                <a href="#" class="btn btn--gold btn--lg">
                                    <span class="btn__text type--uppercase">Learn More</span>
                                </a>
                               
                            </div>
                        </div>
                        
                    </div>
                </li>
                <li class="imagebg image--dark overlay-green" data-overlay="8" >
                    <div class="background-image-holder"> 
                        <img alt="background" src="assets/img/hero-banner-6.jpg"> 
                    </div>
                            
                    <div class="container pos-vertical-center text-center-xs side-image">
                        <div class="row align-items-center mt--20">
                            <div class="col-12 col-md-6 col-lg-7">
                                <p class="text-top mb-xs-10 type--uppercase">Public Presentation and Panel Discussion</p>
                                <h1 class="header-title">The Digital Creative Economy: Copyright, Law and Policy in Africa </h1>
                               <a href="#" class="btn btn--gold btn--lg">
                                    <span class="btn__text type--uppercase">Learn More</span>
                                </a>
                            </div>
                            <div class="col-12 col-md-5 col-lg-5 text-center-xs">
                                <img src="assets/img/banner-img-2.png">
                            </div>
                            
                        </div>
                        
                    </div>
                </li>
            </ul>
            
        </section>

        <section class="unpad banner-boxes">
            <div class="row row--gapless">
                <div class="col-md-4">
                    <a href="meet-experts">
                        <div class="feature feature-2 boxed boxed--border"> 
                            <i class="fi flaticon-team icon first"></i>
                            <div class="feature__body">
                                <p class="type--uppercase unmarg--bottom"> Meet Experts</p>
                                <h1 class="box-title type--bold">Our Team</h1>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="our-services">
                        <div class="feature feature-2 boxed boxed--border"> 
                            <i class="fi flaticon-gear icon"></i>
                            <div class="feature__body">
                                <p class="type--uppercase unmarg--bottom"> Explore our</p>
                                <h1 class="box-title type--bold">Services</h1>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#">
                        <div class="feature feature-2 last boxed boxed--border"> 
                             <i class="fi flaticon-add-user icon"></i>
                            <div class="feature__body">
                                <p class="type--uppercase unmarg--bottom"> Book Free</p>
                                <h1 class="box-title type--bold">Appointment</h1>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </section>

        <section class="expert">
            <div class="container">
                <div class="row row-ml-2 mb--40">
                    
                    <div class="col-12 col-md-12 col-lg-6">
                        <h2 class="header-title color--primary fancy"><span>The Penguide</span></h2>
                    </div>
                </div>
                <div class="row row-ml-2 align-items-center box-expert">
                    
                    <div class="col-12 col-sm-5 col-lg-4 mb-xs-40">
                        <div class="feature feature-1 unmarg--bottom"> <img alt="Image" src="assets/img/expert-1.jpg">
                            <div class="feature__body boxed boxed--border unmarg--bottom">
                                <a href="#">
                                    <h3 class="unmarg--bottom name fancy"><span> Dr. Chijioke Okorie</span></h3>
                                    <p class="type--uppercase ml--30"> Lagos, Nigeria </p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-6 offset-lg-1 text-justify">
                        <p>The Penguide is the online persona of  <strong>Dr. Chijioke Ifeoma Okorie</strong>.</p>
                        <p class="mb--35">Chijioke Okorie is currently the inaugural postdoctoral research fellow under the South African Research Chair in Intellectual Property, Innovation and Development at the University of Cape Town, South Africa. Before becoming a full time researcher and policy consultant, she was a Senior Associate in the law firm of CLP Legal (formerly, Consolex Legal Practitioners).</p>
                        <a href="about-us" class="btn btn--gold">
                            <span class="btn__text type--uppercase">Learn More</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="our-works">
            <div class="container">
                <div class="row row-ml-2 mb--40 mb-sm-10 mb-xs-10">
                    
                    <div class="col-12 col-md-12 col-lg-6">
                        <h2 class="header-title color--primary fancy mb-sm-10 mb-xs-10"><span>Our Work</span></h2>
                    </div>
                </div>
                <div class="row row-ml-2 align-items-center box-expert">
                    
                    <div class="col-12 col-lg-4 mb-sm-40 mb-xs-40 text-justify">
                        <p>Penguide Advisory recognizes the value of IP for creators and users. We understand the media and entertainment sector and the digital economy, particularly in the African context. </p>
                        <p class="mb--35">We provide cutting edge analysis and insights for countries, individuals and organisations with whom we work. Penguide Advisory deploys its expertise in the following areas:</p>
                        <a href="our-services" class="btn btn--gold">
                            <span class="btn__text type--uppercase">View All</span>
                        </a>
                    </div>
                    <div class="col-12 col-lg-7 offset-lg-1">
                        <ul class="accordion accordion-1 accordion--oneopen">
                            <li class="active">
                                <div class="accordion__title">
                                    <span class="h5">Intellectual Property Including</span>
                                </div>
                                <div class="accordion__content">
                                    <div class="content-row">
                                        <div class="first">
                                            <p>Copyright and related rights</p>
                                            <p>Designs</p>
                                            <p>Trademarks</p>
                                            <p>Trade secrets</p>
                                        </div>
                                         <div class="second">
                                            <p>Proprietary confidential information</p>
                                            <p>Patents</p>
                                            <p>Transactional IP</p>
                                            <p>Trade secrets</p>
                                         </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">Media, Entertainment and Sport </span>
                                </div>
                                <div class="accordion__content">
                                    <div class="content-row">
                                        <div class="first">
                                            <p>Music</p>
                                            <p>TV and Films</p>
                                            <p>Games</p>
                                        </div>
                                         <div class="second">
                                            <p>Social media</p>
                                            <p>Sport</p>
                                            <p>Broadcasting</p>
                                         </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="accordion__title">
                                    <span class="h5">Technology and Communications  </span>
                                </div>
                                <div class="accordion__content">
                                    <div class="content-row">
                                        <div class="first">
                                            <p>Software and services</p>
                                            <p>Digital communications</p>
                                            <p>Fintech</p>
                                            <p>Edtech and Agritech</p>
                                        </div>
                                         
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="space--md imagebg" data-overlay="6">
            <div class="background-image-holder"> <img alt="background" src="assets/img/banner-1.jpg"> </div>
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-md-10 col-lg-7">
                        <p class="lead type--uppercase text-top mb-xs-10 color-gold">Get Special Training Now</p>
                        <h1 class="header-title">Explore Trends and changes in the Information Industry.</h1>
                        <a href="#" class="btn btn--border btn--lg">
                            <span class="btn__text color-gold type--uppercase">Sign Up Now</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="projects">
            <div class="container">
                <div class="row">
                    
                    <div class="col-12 col-lg-8">
                        <h2 class="title header-title fancy color--primary"><span>Projects and Publications</span></h2>
                    </div>
                    
                </div>
                <div class="row mb--80">
                    
                    <div class="col-12 col-lg-5">
                        <p class="mb--40">We help rights holder digitally transform their business and reinvent the value of their content.</p>
                        <a href="#" class="btn btn--gold">
                            <span class="btn__text type--uppercase">View All</span>
                        </a>
                    </div>
                    
                </div>

                
            </div>
            <div class="masonry masonry--gapless mt--80">
                    
                <div class="masonry__container bg--white row row--gapless masonry--active">
                    <div class="col-md-3 col-12 masonry__item">
                        <a href="#" class="block">
                            <div class="feature feature-7 boxed text-center imagebg" data-overlay="8">
                                <div class="background-image-holder"> <img alt="background" src="assets/img/case-3.jpg"> </div>
                                <div class="project-thumb-title my-auto">
                                    <div class="mx-auto text-center">
                                        <p class="text-top type--uppercase">Innovation</p>
                                        <h2 class="type--bold">South African Research</h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-12 masonry__item">
                        <a href="#" class="block">
                            <div class="feature feature-7 boxed text-center imagebg" data-overlay="8">
                                <div class="background-image-holder"> <img alt="background" src="assets/img/case-2.jpg"> </div>
                                <div class="project-thumb-title my-auto">
                                    <div class="mx-auto text-center">
                                        <p class="text-top type--uppercase">Metrics, Laws, & Policies </p>
                                        <h2 class="type--bold">Queen Elizabeth Scholar </h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-12 masonry__item">
                        <a href="#" class="block">
                            <div class="feature feature-7 boxed text-center imagebg" data-overlay="8">
                                <div class="background-image-holder"> <img alt="background" src="assets/img/case-5.jpg"> </div>
                                <div class="project-thumb-title my-auto">
                                    <div class="mx-auto text-center">
                                        <p class="text-top type--uppercase">ARTISTS & STARTUPS</p>
                                        <h2 class="type--bold">Legal Aid <br>Initiatives</h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-12 masonry__item">
                        <a href="#" class="block">
                            <div class="feature feature-7 boxed text-center imagebg" data-overlay="8">
                                <div class="background-image-holder"> <img alt="background" src="assets/img/case-4.jpg"> </div>
                                <div class="project-thumb-title my-auto">
                                    <div class="mx-auto text-center">
                                        <p class="text-top type--uppercase">CONVERSATION</p>
                                        <h2 class="type--bold">Copyright, Law & Policy in Africa</h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        
        <section class="contact">
            <div class="container">
                <div class="row justify-content-between row-ml-2 mb--40">
                    
                    <div class="col-12 col-lg-4">
                        <h2 class="title header-title fancy color--primary"><span>Contact Us</span></h2>
                        <ul>
                            <li class="mb--16">
                                <i class="icon material-icons icon--xs mr--8 color-gray">phone</i> +234-14531010
                            </li>
                            <li class="mb--16">
                                <i class="icon material-icons icon--xs mr--8 color-gray">email</i> info@penguideng.com
                            </li>
                            <li class="mb--16">
                                <i class="icon material-icons icon--xs mr--8 color-gray">pin_drop</i>  P.O Box 55627, Ikoyi
                            </li>
                        </ul>
                    </div>
                    
                </div>
                <form class="row ml--30 unmarg--left justify-content-center" action="//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for subcribing.  Please check your inbox." data-error="Please provide your, email address and message.">
                    <div class="col-lg-5 col-md-6"> <input class="validate-required" type="text" name="EMAIL" placeholder="Email Address"> </div>
                    <div class="col-lg-5 col-md-6"> <input class="validate-required validate-email" type="email" name="MESSAGE" placeholder="Message"> </div>
                    
                    <div class="col-lg-2 col-md-6"> <button type="submit" class="btn btn--lg btn-dark-primary type--uppercase">Subscribe Now</button> </div>
                    
                    
                </form>
            </div>
        </section>

        <section class="featured-blogs bg-primary">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <h2 class="title header-title color-gold fancy"><span>Featured Blogs</span></h2>
                    </div>
                </div>

                <div class="row  mb--40 text-center">
                    <div class="col-md-4">
                        <div class="feature feature-1"> 
                            <img alt="Image" src="assets/img/blog-1.jpg">
                            <div class="feature__body boxed boxed--border">
                                <h3>The African Regional Intellectual Property Organisation </h3>
                                <a href="#" class="type--uppercase">
                                    Read More
                                </a> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature feature-1"> 
                            <img alt="Image" src="assets/img/blog-2.jpg">
                            <div class="feature__body boxed boxed--border">
                                <h3>Kenya amends its Copyright Act to ratify the Marrakesh Treaty</h3>
                                <a href="#" class="type--uppercase">
                                    Read More
                                </a> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature feature-1"> 
                            <img alt="Image" src="assets/img/blog-3.jpg">
                            <div class="feature__body boxed boxed--border">
                                <h3>Who owns copyright in Uganda’s National <br>Anthem? </h3>
                                <a href="#" class="type--uppercase">
                                    Read More
                                </a> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-12">
                        <a class="btn btn--gold">
                            <span class="btn__text type--uppercase">View More</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="upcoming-events pad-top">
            <div class="container">
                <div class="row mb--40">
                    <div class="col-12 col-md-7 col-lg-5">
                        <h2 class="title header-title color-gold fancy"><span>Upcoming Events</span></h2>
                        <p class="mb--40">We help rights holder digitally transform their business and reinvent the value of their content.</p>
                        <h2 class="color--primary">2020 Schedules</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-8 mb--40">
                        <div class="calendar-row mb--20">
                            <div class="dates">
                                <div class="boxed">
                                    
                                    <h3 class="unmarg--bottom color--primary">
                                        <span class="number unmarg--bottom">
                                            12 
                                        </span>
                                        Feb
                                    </h3>
                                </div>
                            </div>
                            <div class="event-title">
                                <p class="lead type--uppercase">Public Presentation/Launch of Multi-sided music platforms and the Law</p>
                            </div>
                        </div>
                        <div class="calendar-row mb--20">
                            <div class="dates">
                                <div class="boxed">
                                    
                                    <h3 class="unmarg--bottom color--primary">
                                        <span class="number unmarg--bottom">
                                            24-28 
                                        </span> Feb
                                    </h3>
                                </div>
                            </div>
                            <div class="event-title">
                                <p class="lead type--uppercase">IP Law Clinic - Freeme Digital in collaboration with The Initiative </p>
                            </div>
                        </div>
                        <div class="calendar-row mb--20">
                            <div class="dates">
                                <div class="boxed">
                                    
                                    <h3 class="unmarg--bottom color--primary">
                                        <span class="number unmarg--bottom">
                                            11 
                                        </span> Mar
                                    </h3>
                                </div>
                            </div>
                            <div class="event-title">
                                <p class="lead type--uppercase">Panel Discussion on “The Digital Creative Economy: Copyright, Law and Policy in Africa”</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-12 col-lg-8">
                        <a class="btn btn--gold">
                            <span class="btn__text type--uppercase">
                                View All
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        
        

        

       
<?php include "elements/footer.php";?>